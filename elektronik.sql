-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2018 at 05:01 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elektronik`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(3) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `foto` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `nama`, `username`, `password`, `foto`) VALUES
(1, 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'administrator.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer`
--

CREATE TABLE `tb_customer` (
  `id_customer` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_depan` varchar(50) NOT NULL,
  `nama_belakang` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_customer`
--

INSERT INTO `tb_customer` (`id_customer`, `id_user`, `nama_depan`, `nama_belakang`, `email`, `no_hp`, `alamat`) VALUES
(1, 0, 'Aldi Saputra', 'Wahyudi', 'aldisawah@gmail.com', '089667997444', 'Jalan Pelesiran'),
(2, 0, 'Aldi', 'Sapardi', 'sadad@sadad.sadad', '089667997464', 'Jalan Jalan'),
(3, 0, 'Prayoga', 'Ildhan', 'agoybagoy@gmail.com', '08927231361', 'Jalan Pasteur no 21C');

-- --------------------------------------------------------

--
-- Table structure for table `tb_elektronik`
--

CREATE TABLE `tb_elektronik` (
  `id` int(3) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `merk` int(3) NOT NULL,
  `kategori` int(3) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(3) NOT NULL,
  `deskripsi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_elektronik`
--

INSERT INTO `tb_elektronik` (`id`, `nama_produk`, `merk`, `kategori`, `warna`, `gambar`, `harga`, `stok`, `deskripsi`) VALUES
(7, 'Speaker Logitech Z906 ', 5, 3, 'Hitam', 'img_1543419260.jpg', 4850000, 91, 'These surround sound speakers have met strict performance standards to achieve THX certification.  Package Contents Five satellite speakers Subwoofer  Speaker connection Wire 6-foot (1.82-m) six-channel direct cable  Stackable control consol'),
(11, 'Xiaomi MI LED 4A 32inch TV Android Smart TV', 8, 2, 'Hitam', 'img_1543415569.jpg', 2149000, 29, '-Prosesor Amlogic quad-core 64-bit -Remote kontrol dengan 12 tombol, mudah untuk digunakan -Slot untuk 3 HDMI, 2 USB, dan koneksi Bluetooth Wifi -DTS + Speaker stereo, untuk pengalaman menonton seperti nyata -HD (1366*768) -Al Speech, untuk voice command'),
(13, 'LG DP132 USB DVD Player', 6, 6, 'Hitam', 'img_1543415926.jpg', 389900, 84, 'LG mengenalkan DVD Player seri DP132 yang telah dibekali dengan berbagai fitur yang mampu menyuguhkan kualitas gambar yang lebih baik. Selain mampu menampilkan gambar yang berkualitas ternyata LG juga membekali DVD Player ini dengan berbagai kemudahan unt'),
(14, 'POLYTRON 2291G HDMI', 4, 6, 'Hitam', 'img_1543416019.jpg', 525000, 84, 'DVD Player 2291 HD Free Kabel HDMI 1.5 Meter Resolusi 1080  - DVD : Single Disc Front Loading - USB 2.0 Input : Yes - Flash Rip (CD Ripg) : Yes - MIC Input : Yes - Product Dimension (mm) : p x l x t = 360 x 245 x 45 mm'),
(15, 'Polytron Big Band BB-5510', 4, 3, 'Hitam', 'img_1543416176.jpg', 2880000, 34, 'Specification - Display : LED and VFD - USB 2.0 (input) : Yes - DVD : Yes - Remote Control : Yes - Mic Input with Mic Level & Digital Echo : Yes - Radio FM PLL : Yes - Bluetooth A2DP Streaming Input : Yes - Preset Equalizer : Yes - Bazzoke (Bass Booster) '),
(16, 'Home Theatre LG DH3140', 6, 3, 'Hitam', 'img_1543416272.jpg', 1395000, 46, 'LG DVD HOME THEATER 5.1 DH 3140 dengan teknologi baru dengan suara yang jernih dan jelas bisa anda rasakan seperti menonton di seluruh sistem home theater dirancang untuk membenamkan Anda dalam dunia kenikmatan multimedia, dan surround'),
(17, ' Vehicle GPS Tracking Device GSM GPRS GPS Tracker ', 1, 5, 'Hitam', 'img_1543416357.jpg', 300000, 58, 'GPS Tracking Device berguna untuk mentracking posisi gps device ini.  Anda dapat menaruhnya di mobil, tas, dll dan jika hilang, Anda dapat melacak posisi gps device ini secara real time sehingga Anda selalu mengetahui dimana posisi device ini.'),
(18, 'BENQ MX 505 A Original ISPCOM', 10, 7, 'Hitam', 'img_1543416432.jpg', 6060000, 72, 'BenQ MX505A cocok digunakan untuk keperluan presentasi kelas atau belajar mengajar karena dilengkapi fitur seperti Teaching Template yang membuat proses mengajar menjadi lebih praktis. Selain itu resolusi 1024x768 yang dimiliki membuat presentasi yang ter'),
(19, 'Projector Viewsonic PA500S HDMI', 9, 7, 'Putih', 'img_1543416841.jpg', 4010000, 82, 'Projection System : 0.55\" SVGA Native Resolution : 800x600 DC Type : DC3 Brightness : 3600 ANSI lumens Contrast Ratio With SuperEco Mode : 22000:1 Display Color : 1.07 Billion Colors Light Source : Lamp Light Source Life (Nor/SuperEco) : 5000/15000 Lamp W'),
(20, 'TV LED PANASONIC 49F306', 7, 2, 'Hitam', 'img_1543417010.jpg', 6350000, 91, 'Fitur : -LED TV -49 Inch -FULL HD (1920x1080p) -IPS LED -Panel Drive 200 Hz BMR -DOT NOISE REDUCTION -Digital TV (DVB-T2) -USB Movie -2x HDMI -1x AV in -1x USB'),
(21, 'Sony Portable ICD UX 560 F 4GB Voice Recoder Stere', 11, 8, 'Emas', 'img_1543417880.jpg', 1400000, 21, 'Built-in Memory4 GBPC ConnectivityYesBuilt-in MicrophoneStereo (S-Mic)Recording FormatLinear PCM/MP3Playback FormatLPCM/MP3/AAC/WMAMaximum Number of File5000 files (including number of folder)Maximum Number of Files in One Folder199Calendar SearchYesLCD B'),
(22, ' Digital Voice Recorder 8GB R29 RG109540', 1, 8, 'Hitam', 'img_1543418101.jpg', 199000, 12, 'Jika Anda sering melakukan rapat / meeting, membahas hal-hal penting, persidangan ataupun pengintaian, perekam suara atau voice recorder ini menjadi pilihan terbaik, dapat merekam suara dengan jelas dan jernih dengan teknologi yang dikhususkan pada penang'),
(23, 'MultiPack MyCare LedBulb 10W 6500K', 12, 9, 'Putih', 'img_1544276063.png', 165000, 27, 'Nyalakan Philips LED setiap saat Mengapa Anda memilih LED?  Lampu LED menciptakan kualitas cahaya sempurna untuk setiap suasana. Dari cahaya romantis untuk malam yang santai hingga cahaya cerah untuk menerangi objek khusus. Lampu LED menawarkan segala yan'),
(24, 'LED SOROT 20W Flood Light Outdoor', 1, 9, '-', 'img_1544276263.jpg', 75000, 11, 'Spesifikasi :  - 20W SMD LED  - IP65 Outdoor  - Hemat energi 80% - Warna : White  - Voltage : 85-265V - Ukuran : 17,5 x 13,5 x 4 cm');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` int(3) NOT NULL,
  `nama_kategori` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Elektronik Lainnya'),
(2, 'Televisi'),
(3, 'Home Theater'),
(5, 'GPS'),
(6, 'Media Player'),
(7, 'Proyektor'),
(8, 'Voice Recorder'),
(9, 'Pencahayaan');

-- --------------------------------------------------------

--
-- Table structure for table `tb_merk`
--

CREATE TABLE `tb_merk` (
  `id_merk` int(3) NOT NULL,
  `nama_merk` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_merk`
--

INSERT INTO `tb_merk` (`id_merk`, `nama_merk`) VALUES
(1, 'Merk Lainnya'),
(2, 'Samsung'),
(4, 'Polytron'),
(5, 'Logitech'),
(6, 'LG'),
(7, 'Panasonic'),
(8, 'Xiaomi'),
(9, 'ViewSonic'),
(10, 'BENQ'),
(11, 'Sony'),
(12, 'Philips');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `id_order` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `order_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`id_order`, `id_user`, `id_customer`, `order_total`) VALUES
(1, 1, 1, 525000),
(2, 1, 2, 990000),
(3, 2, 3, 75000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_orderdetail`
--

CREATE TABLE `tb_orderdetail` (
  `id_orderdetail` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `stok_terjual` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_orderdetail`
--

INSERT INTO `tb_orderdetail` (`id_orderdetail`, `id_order`, `id_produk`, `stok_terjual`) VALUES
(1, 1, 14, 1),
(2, 2, 23, 6),
(3, 3, 24, 1);

--
-- Triggers `tb_orderdetail`
--
DELIMITER $$
CREATE TRIGGER `penjualan` AFTER INSERT ON `tb_orderdetail` FOR EACH ROW BEGIN
	UPDATE tb_elektronik SET stok=stok-NEW.stok_terjual
    WHERE id = NEW.id_produk;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(3) NOT NULL,
  `nama_depan` varchar(255) NOT NULL,
  `nama_belakang` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama_depan`, `nama_belakang`, `username`, `email`, `password`) VALUES
(1, 'user', 'dummy', 'user', 'user@email@yahoo.com', 'ee11cbb19052e40b07aac0ca060c23ee'),
(2, 'Aldi Saputra', 'Wahyudi', 'aldisaw', 'aldi.163040020@mail.unpas.ac.id', 'c75674b8b5f881db4b48c6fc57293e7f'),
(3, 'Prayoga', 'Ildhan', 'agoy', 'prayogaildhan@gmail.com', '5b3c2c50892bc0e428f8e05255178ba9');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_customer`
--
ALTER TABLE `tb_customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `tb_elektronik`
--
ALTER TABLE `tb_elektronik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tb_merk`
--
ALTER TABLE `tb_merk`
  ADD PRIMARY KEY (`id_merk`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `tb_orderdetail`
--
ALTER TABLE `tb_orderdetail`
  ADD PRIMARY KEY (`id_orderdetail`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_customer`
--
ALTER TABLE `tb_customer`
  MODIFY `id_customer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_elektronik`
--
ALTER TABLE `tb_elektronik`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kategori` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_merk`
--
ALTER TABLE `tb_merk`
  MODIFY `id_merk` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_orderdetail`
--
ALTER TABLE `tb_orderdetail`
  MODIFY `id_orderdetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
