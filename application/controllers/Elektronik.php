<?php

require APPPATH . '/libraries/REST_Controller.php';

class Elektronik extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Elektronik_model", "elektronik");
    }

    public function index_get()
    {
        $id = $this->get('id');
        $cari = $this->get('cari');
        
        if ($cari != "") {
            $elektronik = $this->elektronik->cari($cari)->result();
        } elseif ($id == '') {
            $elektronik = $this->elektronik->getData(null)->result();
        } else{
            $elektronik = $this->elektronik->getData($id)->result();
        }

        $this->response($elektronik);
        // ketikan source code yang ada di modul
    }

    public function index_put()
    {
        $nama_produk = $this->put('nama_produk');
        $merk = $this->put('merk');
        $kategori = $this->put('kategori');
        $warna = $this->put('warna');
        $gambar = $this->put('gambar');
        $harga = $this->put('harga');
        $stok = $this->put('stok');
        $deskripsi = $this->put('deskripsi');
        $id = $this->put('id');
        $data = array(
            'nama_produk' => $nama_produk,
            'merk' => $merk,
            'kategori' => $kategori,
            'warna' => $warna,
            'gambar' => $gambar,
            'harga' => $harga,
            'stok' => $stok,
            'deskripsi' => $deskripsi,
        );
        $update = $this->elektronik->update('tb_elektronik', $data, 'id', $this->put('id'));

        if ($update) {
            $this->response(array('status' => 'success', 200));
        } else{
            $this->response(array('status' => 'fail', 502));
        }
        // ketikan source code yang ada di modul
    }

    public function index_post()
    {
        $nama_produk = $this->post('nama_produk');
        $merk = $this->post('merk');
        $kategori = $this->post('kategori');
        $warna = $this->post('warna');
        $gambar = $this->post('gambar');
        $harga = $this->post('harga');
        $stok = $this->post('stok');
        $deskripsi = $this->post('deskripsi');
        $data = array(
            'nama_produk' => $nama_produk,
            'merk' => $merk,
            'kategori' => $kategori,
            'warna' => $warna,
            'gambar' => $gambar,
            'harga' => $harga,
            'stok' => $stok,
            'deskripsi' => $deskripsi,
        );

        $insert = $this->elektronik->insert($data);
        if ($insert) {
            $this->response(array('status' => 'success', 200));
        }else{
            $this->response(array('status' => 'fail', 502));
        }
        // ketikan source code yang ada di modul
    }

    public function index_delete()
    {
        $id = $this->delete('id');
        $delete = $this->elektronik->delete('tb_elektronik', 'id', $id);
        if ($delete) {
            $this->response(array('status' => 'success', 201));
        }else{
            $this->response(array('status' => 'fail', 502));
        }
       // ketikan source code yang ada di modul
    }
}