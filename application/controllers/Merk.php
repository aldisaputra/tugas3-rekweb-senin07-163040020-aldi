<?php

require APPPATH . '/libraries/REST_Controller.php';

class Merk extends REST_Controller {

	public function __construct($config = "rest")
	{
		parent::__construct($config);
		$this->load->model("Merk_model");
	}

    public function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $merk = $this->Merk_model->getAll(NULL)->result();
        } else {
            $merk = $this->Merk_model->getAll($id)->result();
        }
        $this->response($merk, 200);
    }

    public function index_put() {
        $id = $this->put('id_merk');
        $data = array(
            'nama_merk' => $this->put('nama_merk'),
            );
        $update = $this->Merk_model->update('tb_merk', $data, 'id_merk', $this->put('id_merk'));
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
    public function index_post() {
        $data = array(
            'nama_merk' => $this->post('nama_merk'),
            );
        $insert = $this->Merk_model->insert($data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete() {
        $id = $this->delete('id_merk');
        $delete = $this->Merk_model->delete('tb_merk', 'id_merk', $id);
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}