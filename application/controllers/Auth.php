<?php

require APPPATH . '/libraries/REST_Controller.php';

class Auth extends REST_Controller {

	public function __construct($config = "rest")
	{
		parent::__construct($config);
		$this->load->model("Login_model");
	}
    public function registration_post() {
        $data = array(
                'nama_depan' => $this->post('nama_depan'),
				'nama_belakang' => $this->post('nama_belakang'),
				'username' => $this->post('username'),
				'email' => $this->post('email'),
				'password' => md5($this->post('password'))
            );
        $insert = $this->Login_model->registration_insert($data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    	// Check for user login process
	public function user_login_process_post() {
			$data = array(
				'username' => $this->post('username'),
				'password' => md5($this->post('password'))
			);
			$result = $this->Login_model->login($data);
			if ($result) {
                    $this->response($data, 200);
			} 
			else {
				$this->response(array('status' => 'fail', 502));
			}
    }

    public function read_user_post() {
		$username = $this->post('username');
        $result = $this->Login_model->read_user_information($username);
        if ($result) {
                $this->response($username, 200);
        } 
        else {
            $this->response(array('status' => 'fail', 502));
        }
    }
    
    public function admin_post() {
        $data = array(
            'username' => $this->post('username'),
            'password' => md5($this->post('password'))
        );
        $result = $this->Login_model->login_admin($data);
        if ($result) {
                $this->response($data, 200);
        } 
        else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function read_admin_post() {
		$username = $this->post('username');
        $result = $this->Login_model->read_admin_information($username);
        if ($result) {
                $this->response($data, 200);
        } 
        else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}