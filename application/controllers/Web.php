<?php

require APPPATH . '/libraries/REST_Controller.php';

class Web extends REST_Controller {

	public function __construct($config = "rest")
	{
		parent::__construct($config);
		$this->load->model("Web_model");
	}

    public function index_get() {
        $web = $this->Web_model->getAllProduct(null)->result();
        $this->response($web, 200);
    }

    public function detail_get() {
        $id = $this->get('id');
        $web = $this->Web_model->getAllProduct($id)->row();
        $this->response($web, 200);
    }

    public function rekomen_get() {
        $rekomen = $this->Web_model->getRekomen()->result();
        $this->response($rekomen, 200);
    }

    public function produk_get() {
        $kategori = $this->get('kategori');
        $produk = $this->Web_model->getProduct($kategori)->result();
        $this->response($produk, 200);
    }

    public function save_customer_post() {
        $data = array();
        $data['nama_depan'] = $this->post('nama_depan');
        $data['nama_belakang'] = $this->post('nama_belakang');
        $data['email'] = $this->post('email');
        $data['no_hp'] = $this->post('no_hp');
        $data['alamat'] = $this->post('alamat');
        $insert = $this->Web_model->save_customer($data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function save_order_post() {
       $odata = array();
        $odata['id_user'] = $this->session->userdata['logged_in']['id_user'];
        $odata['id_customer'] = $this->session->userdata('id_customer');
        $odata['order_total'] = $this->cart->total();

        $order_id = $this->Web_model->save_order($odata);
        if ($order_id) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function save_order_details_post() {
        $oddata = array();
        $oddata['id_order'] = $order_id;
        $oddata['id_produk'] = $oddatas['id'];
        $oddata['stok_terjual'] = $oddatas['qty'];
 
         $inser =  $this->Web_model->save_order_details($oddata);
         if ($insert) {
             $this->response($data, 200);
         } else {
             $this->response(array('status' => 'fail', 502));
         }
     }
}