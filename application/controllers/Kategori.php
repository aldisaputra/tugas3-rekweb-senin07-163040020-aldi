<?php

require APPPATH . '/libraries/REST_Controller.php';

class Kategori extends REST_Controller {

	public function __construct($config = "rest")
	{
		parent::__construct($config);
		$this->load->model("Kategori_model");
	}

    public function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $kategori = $this->Kategori_model->getKategori(NULL)->result();
        } else {
            $kategori = $this->Kategori_model->getKategori($id)->result();
        }
        $this->response($kategori, 200);
    }

    public function index_put() {
        $id = $this->put('id_kategori');
        $data = array(
            'nama_kategori' => $this->put('nama_kategori')
            );
        $update = $this->Kategori_model->update('tb_kategori', $data, 'id_kategori', $this->put('id_kategori'));
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
    public function index_post() {
        $data = array(
            'nama_kategori' => $this->post('nama_kategori'),
            );
        $insert = $this->Kategori_model->insert($data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete() {
        $id = $this->delete('id_kategori');
        $delete = $this->Kategori_model->delete('tb_kategori', 'id_kategori', $id);
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}