<?php

class Web_Model extends CI_Model {

     public function __construct()
    {
        parent::__construct();
    }

     public function getAllProduct($id_elektronik = null){
        $this->db->select('id, nama_produk, tb_merk.nama_merk as merk, tb_kategori.nama_kategori as kategori, warna, gambar,deskripsi, harga, stok');
        $this->db->from('tb_elektronik');
        $this->db->join('tb_kategori', 'tb_elektronik.kategori = tb_kategori.id_kategori');
        $this->db->join('tb_merk', 'tb_elektronik.merk = tb_merk.id_merk');

        if ($id_elektronik == null) {
            $this->db->order_by('id', 'asc');
        }else {
            $this->db->where('id', $id_elektronik);
        }

        return $this->db->get();
    }

    public function getProduct($kategori){
        $this->db->select('id, nama_produk, tb_merk.nama_merk as merk, tb_kategori.nama_kategori as kategori, gambar, harga');
        $this->db->from('tb_elektronik');
        $this->db->join('tb_kategori', 'tb_elektronik.kategori = tb_kategori.id_kategori');
        $this->db->join('tb_merk', 'tb_elektronik.merk = tb_merk.id_merk');
        $this->db->where('kategori', $kategori);
        
        return $this->db->get();
    }
    public function getRekomen(){
        $this->db->select('id, nama_produk, tb_merk.nama_merk as merk, tb_kategori.nama_kategori as kategori, gambar, harga');
        $this->db->from('tb_elektronik');
        $this->db->join('tb_kategori', 'tb_elektronik.kategori = tb_kategori.id_kategori');
        $this->db->join('tb_merk', 'tb_elektronik.merk = tb_merk.id_merk');
        $this->db->order_by('id', 'random');
        $this->db->limit(3);
        
        return $this->db->get();
    }

    public function save_customer($data) {
        $this->db->insert('tb_customer', $data);
        return $this->db->insert_id();
    }

    public function save_order($data) {
        $this->db->insert('tb_order', $data);
        return $this->db->insert_id();
    }

    public function save_order_details($oddata){
        $this->db->insert('tb_orderdetail', $oddata);
    }

}
