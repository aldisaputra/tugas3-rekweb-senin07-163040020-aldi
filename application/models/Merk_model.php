<?php

class Merk_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll($id_merk = null){
		$this->db->select('*');
		$this->db->from('tb_merk');

		if ($id_merk != null) {
			$this->db->where('id_merk', $id_merk);
		}

		return $this->db->get();
	}

	public function insert($data){
		$this->db->insert('tb_merk', $data);
		return $this->db->affected_rows();
	}

    public function update($table, $data, $par, $var) {
        $this->db->update($table, $data, array($par => $var));
        return $this->db->affected_rows();
    }

    public function delete($table, $par, $var){
        $this->db->where($par, $var);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
}
