<?php

class Elektronik_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

    public function getData($id = null){
        // ketikan source code yang ada di modul
        $this->db->select('id, nama_produk, tb_merk.nama_merk as merk, tb_kategori.nama_kategori as kategori, warna, gambar, harga, deskripsi, stok');
		$this->db->from('tb_elektronik');
		$this->db->join('tb_kategori', 'tb_elektronik.kategori = tb_kategori.id_kategori');
		$this->db->join('tb_merk', 'tb_elektronik.merk = tb_merk.id_merk');
        
        if ($id == null) {
            $this->db->order_by('id', 'asc');
        } else {
            $this->db->where('id', $id);
        }

        return $this->db->get();
    }

    public function cari($cari)
    {
        // ketikan source code yang ada di modul
        $this->db->select('id, nama_produk, tb_merk.nama_merk as merk, tb_kategori.nama_kategori as kategori, warna, gambar, harga, deskripsi, stok');
		$this->db->from('tb_elektronik');
		$this->db->join('tb_kategori', 'tb_elektronik.kategori = tb_kategori.id_kategori');
		$this->db->join('tb_merk', 'tb_elektronik.merk = tb_merk.id_merk');
        $this->db->like('nama_produk', $cari);
        $this->db->or_like('nama_merk', $cari);
        $this->db->or_like('nama_kategori', $cari);
        $this->db->or_like('deskripsi', $cari);
        return $this->db->get();
    }

    public function insert($data){
       // ketikan source code yang ada di modul
        $this->db->insert('tb_elektronik', $data);
        return $this->db->affected_rows();
    }

    public function update($table, $data, $par, $var) {
        $this->db->update($table, $data, array($par => $var));
        return $this->db->affected_rows();
    }
    
    public function delete($table, $par, $var){
       // ketikan source code yang ada di modul
        $this->db->where($par, $var);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
}